# dfraw-language README

Enables basic syntax highlighting for Dwarf Fortress raws.

## Features

It adds highlighting. That's it.

## Requirements

None.

## Extension Settings

None.

## Known Issues

Highlighting is incomplete

## Release Notes

### 0.3.0

Initial public release